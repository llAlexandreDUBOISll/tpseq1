package programmes;

import java.util.Scanner;

public class Exo4 {
    public static void main (String[] args) {
        double ca, com=0.0;
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Veuillez entrer votre chiffre d'affaire : ");
        ca=scan.nextFloat();
        
        if (0<=ca && ca<10000){
            com=ca*0.02;
        } else if (10000<=ca && ca<20000) {
            com=200+(ca-10000)*0.04;
        } else if (20000<=ca) {
            com=600+(ca-20000)*0.06;
        }
        System.out.println("La commission est de : " + com);
    }
}
