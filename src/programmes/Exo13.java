
package programmes;

import java.util.Random;
import java.util.Scanner;

public class Exo13 {

    public static void main(String[] args) {
        Scanner  scan   = new Scanner(System.in);
        
        boolean  trouve = false;
        
        long debut;
        long tempsEcoulMillis;
        int nbADeviner;
        int nbPropose;
        int nbEssais=0;
        
        float tempsEcoulSec;
        
        debut = System.currentTimeMillis();
               
        Random rd=new Random();
        nbADeviner=rd.nextInt(1000)+1;
        System.out.println("Proposez un nombre entre 1 et 1000");
        System.out.println("Essayez de le deviner avec le moins d'essais possible.");
        
        while ( ! trouve ){
        nbEssais++;
            
        nbPropose=scan.nextInt();
        if (nbPropose == 0){
            System.out.println();
            System.out.println("Le nombre a deviner était : "+nbADeviner); 
        trouve=true;}
        else if (nbPropose>nbADeviner){
            System.out.println("Trop grand.");
            System.out.println("Essayez un autre nombre (ou abandonnez en saisissant 0)");
        }
        else if(nbPropose<nbADeviner){
            System.out.println("Trop petit.");
            System.out.println("Essayez un autre nombre (ou abandonnez en saisissant 0)");
        }
       
        else 
            {System.out.println("Bravo vous avez trouvé en "+nbEssais + " essais !");
        trouve=true;}
        }
        
        //bien mettre après la boucle sinon le temps sera de 0.
        tempsEcoulMillis = System.currentTimeMillis() - debut;
        tempsEcoulSec = tempsEcoulMillis/1000f;
        
        System.out.println("");
        System.out.println("Vous avez trouvé en " +tempsEcoulSec +" secondes");
    }
}
